#ifndef OPTION_H
#define OPTION_H

#include <functional>
#include <optional>
#include <string>

namespace opt {

enum KindError { AlreadySet, Positivity };

struct OptionException : public std::exception {
    OptionException(const KindError &kind_, const std::string &id_)
        : std::exception(), kind(kind_), id(id_) {
        switch (kind) {
            case AlreadySet:
                id += " : is already set";
                break;
            case Positivity:
                id += " : must be positive";
                break;
            default:
                id = "The exception is ill defined.";
                break;
        }
    }

    // this produces a tidy warning. But it's not compiling without c++20
    // otherwise.
    [[nodiscard]] const char *what() const noexcept final {
        return id.c_str();
    }

private:
    KindError kind;
    std::string id;
};

template <typename T>
class Option {
public:
    Option();

    explicit Option(std::optional<T> opt_);

    explicit Option(T val);

    friend bool operator==(const Option<T> &l, const Option<T> &r) {
        if (l.has_value() && r.has_value()) {
            return l.value() == r.value();
        }
        return l.has_value() == r.has_value();
    }

    friend bool operator!=(const Option<T> &l, const Option<T> &r) {
        return !(l == r);
    }

    friend Option<T> operator+(const Option<T> &l, const Option<T> &r) {
        if (l.has_value() && r.has_value()) {
            return Option<T>(l.value() + r.value());
        }
        return Option<T>();
    }

    friend Option<T> operator-(const Option<T> &l, const Option<T> &r) {
        if (l.has_value() && r.has_value()) {
            return Option<T>(l.value() - r.value());
        }
        return Option<T>();
    }

    friend Option<T> operator*(const Option<T> &l, const Option<T> &r) {
        if (l.has_value() && r.has_value()) {
            return Option<T>(l.value() * r.value());
        }
        return Option<T>();
    }

    friend Option<T> operator/(const Option<T> &l, const Option<T> &r) {
        if (l.has_value() && r.has_value()) {
            return Option<T>(l.value() / r.value());
        }
        return Option<T>();
    }

    // set option but fail if already contains value
    auto unique_set(T val);

    auto maybe_set(T val);

    auto maybe_set(const Option<T> &val);

    auto maybe_set(Option<T> &&val);

    auto emplace(T val);

    [[nodiscard]] auto value() const;

    [[nodiscard]] auto value_or(T val) const;

    [[nodiscard]] auto has_value() const;

    [[nodiscard]] auto try_value(const std::string &s) const;

    [[nodiscard]] auto to_str() const -> std::string;

    auto map(std::function<T(T)> f) -> Option<T> &;

    auto clone() -> Option<T>;

private:
    std::optional<T> opt;
};

template<typename T>
std::ostream& operator << ( std::ostream& os, Option<T> const& value ) {
    os << value.to_str();
    return os;
}

}  // namespace opt

#endif