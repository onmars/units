#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "../option.h"
#include "../option.hh"
#include "catch.hpp"

TEMPLATE_TEST_CASE("Constructors and (in-)equality operators", "[option][template]", int, double,
                   float) {
    opt::Option<TestType> opt(static_cast<TestType>(2.7));
    REQUIRE(opt == opt::Option<TestType>(static_cast<TestType>(2.7)));
    REQUIRE(opt != opt::Option<TestType>(static_cast<TestType>(3.7)));
    REQUIRE(opt == opt::Option<TestType>(opt));

    opt::Option<TestType> emptyOpt;
    REQUIRE(emptyOpt.value_or(static_cast<TestType>(7.2)) == static_cast<TestType>(7.2));
    REQUIRE(emptyOpt == opt::Option<TestType>());
    REQUIRE(emptyOpt != opt);

    opt::Option<TestType> copyOpt(opt);
    REQUIRE(copyOpt == opt);
    REQUIRE(copyOpt != emptyOpt);

    opt::Option<TestType> copyEmptyOpt(emptyOpt);
    REQUIRE(copyEmptyOpt == emptyOpt);
    REQUIRE(copyOpt != copyEmptyOpt);
}

TEMPLATE_TEST_CASE("Basic interface", "[option][template]", int, double, float) {
    opt::Option<TestType> opt(static_cast<TestType>(2.7));
    REQUIRE(opt.value() == static_cast<TestType>(2.7));
    REQUIRE(opt.value_or(12.1) == static_cast<TestType>(2.7));
    REQUIRE(opt.has_value());

    opt::Option<TestType> emptyOpt;
    REQUIRE(emptyOpt.value_or(static_cast<TestType>(7.2)) == static_cast<TestType>(7.2));
    REQUIRE(!emptyOpt.has_value());

    opt = emptyOpt;
    REQUIRE(opt == emptyOpt);
    REQUIRE(opt == opt);
    REQUIRE(&opt != &emptyOpt);

    opt = opt;
    REQUIRE(opt == opt);
    REQUIRE(opt == emptyOpt);

    opt = opt::Option<TestType>(static_cast<TestType>(12.8));
    REQUIRE(opt.value() == static_cast<TestType>(12.8));

    opt::Option<TestType> clonedOpt = opt.clone();
    REQUIRE(clonedOpt == opt);
    REQUIRE(&clonedOpt != &opt);
    REQUIRE(clonedOpt.to_str() == std::to_string(static_cast<TestType>(12.8)));

    opt::Option<TestType> clonedEmptyOpt = emptyOpt.clone();
    REQUIRE(clonedEmptyOpt == emptyOpt);
    REQUIRE(&clonedEmptyOpt != &emptyOpt);
    REQUIRE(clonedEmptyOpt.to_str() == "none");
}

TEMPLATE_TEST_CASE("Advanced interface", "[option][template]", int, double, float) {
    opt::Option<TestType> opt(static_cast<TestType>(2.7));
    REQUIRE(opt.map([](TestType a) {
        return a;
    }) == opt::Option<TestType>(2.7));
    REQUIRE(opt.map([](TestType a) {
        return 13.8 * a;
    }) == opt::Option<TestType>(13.8 * static_cast<TestType>(2.7)));

    opt::Option<TestType> emptyOpt;
    REQUIRE(emptyOpt.map([](TestType a) {
        return a;
    }) == opt::Option<TestType>());

    REQUIRE(emptyOpt.map([](TestType a) {
        return 13.8 * a;
    }) == opt::Option<TestType>());
}
