#ifndef OPTION_HH
#define OPTION_HH

#include <iostream>

#include "option.h"

namespace opt {

template <typename T>
Option<T>::Option() : opt(std::nullopt) {
}

template <typename T>
Option<T>::Option(std::optional<T> opt_) : opt(opt_) {
}

template <typename T>
Option<T>::Option(T val) : opt(std::optional<T>(val)) {
}

template <typename T>
auto Option<T>::unique_set(T val) {
    if (has_value()) {
        throw OptionException(AlreadySet, "variable");
    }
    emplace(val);

    return *this;
}

template <typename T>
auto Option<T>::maybe_set(T val) {
    if (!has_value()) {
        emplace(val);
    }
    return *this;
}

template <typename T>
auto Option<T>::maybe_set(const Option<T> &val) {
    if (!has_value()) {
        *this = val;
    }
    return *this;
}

template <typename T>
auto Option<T>::maybe_set(Option<T> &&val) {
    if (!has_value()) {
        opt = val.opt;
    }
    return *this;
}

template <typename T>
auto Option<T>::emplace(T val) {
    opt.emplace(val);
    return *this;
}

template <typename T>
auto Option<T>::value() const {
    return opt.value();
}

template <typename T>
auto Option<T>::value_or(T val) const {
    return opt.value_or(val);
}

template <typename T>
auto Option<T>::has_value() const {
    return opt.has_value();
}

template <typename T>
auto Option<T>::try_value(const std::string &s) const {
    T val = T();
    try {
        val = value();
    } catch (const std::bad_optional_access &e) {
        std::cout << s << " : " << e.what() << std::endl;
        throw e;
    }
    return val;
}

template <typename T>
auto Option<T>::to_str() const -> std::string {
    if (has_value()) {
        return std::to_string(value());
    }
    return "none";
}

template <typename T>
auto Option<T>::map(std::function<T(T)> f) -> Option<T> & {
    if (has_value()) {
        opt = std::make_optional(f(*opt));
    }
    return *this;
}

template <typename T>
auto Option<T>::clone() -> Option<T> {
    return Option<T>(opt);
}

}  // namespace opt

#endif